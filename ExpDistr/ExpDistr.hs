import           Control.Applicative
import           Control.Monad
import           Data.Maybe
import           Text.Printf
import           Text.Read                   (readMaybe)

import           Data.List
import           Data.Number.Erf
import           Graphics.Gnuplot.Simple
import qualified Graphics.UI.Threepenny      as UI
import           Graphics.UI.Threepenny.Core

main :: IO ()
main = startGUI defaultConfig { tpPort = 10000, tpStatic = Just "." } setup

densityPicFile = "density-pic.png"
cdfPicFile = "cdf-pic.png"
imgDensID = "imgdens"
imgCdfId = "imgcdf"

mkInputColImage (inInfo, inEl, image, link, image2, link2) =
  column [ row [string inInfo, element inEl]
         , UI.a # set UI.href link #+ [element image]
         , UI.a # set UI.href link2 #+ [element image2]
         ] #. "sample-table"

mkDistImage id = UI.image #. "sample-image"
               # set UI.height 350 # set UI.width 500
               # set UI.id_ id

expDistDensity :: Double -> Double -> Double
expDistDensity l x = l * exp (- l*x)

expDistCDF :: Double -> Double -> Double
expDistCDF l x = 1 - exp (- l*x)

setup :: Window -> IO ()
setup window = void $ do
    return window # set title "Exponential distribution"
    UI.addStyleSheet window "expdistr.css"

    lambdaInput <- UI.input
    imgDensity <- mkDistImage imgDensID
    imgCdf <- mkDistImage imgCdfId
    urlDensity <- loadFile window "image/png" densityPicFile
    urlCdf <- loadFile window "image/png" cdfPicFile

    getBody window #+ [mkInputColImage ("λ: ", lambdaInput, imgDensity, urlDensity, imgCdf, urlCdf)]

    lambdaIn <- stepper "-" $ UI.valueChange lambdaInput
    let readLambda = maybe (return ()) plotAndRefresh . join . fmap (\x -> if x > 0.0 then Just x else Nothing) . readMaybe

        refresh :: (String, String) -> JSFunction ()
        refresh (id, url) = ffi "document.images[%1].src = %2 + '?' + new Date().getTime();" id url
        plotL file fun l = plotFunc [PNG file] (linearScale 100 (0, 4)) (fun l)
        plotAndRefresh l = plotL densityPicFile expDistDensity l
                         >> plotL cdfPicFile expDistCDF l
                         >> mapM_ (\f -> runFunction window (refresh f)) [(imgDensID, urlDensity),
                                                                         (imgCdfId, urlCdf)]

    on UI.valueChange lambdaInput $ const $ do
      lam <- get UI.value lambdaInput
      readLambda lam
