import           Control.Applicative
import           Control.Monad
import           Text.Read                   (readMaybe)

import           Data.List                   (intersperse)
import qualified Graphics.UI.Threepenny      as UI
import           Graphics.UI.Threepenny.Core
import           Numeric.LinearAlgebra       hiding (join)
import           Text.Printf

main :: IO ()
main = startGUI defaultConfig { tpPort = 10000, tpStatic = Just "." } setup

setup :: Window -> IO ()
setup window = void $ do
    return window # set title "Kolmogorov equations"
    UI.addStyleSheet window "statkolm.css"

    matrixInput <- UI.textarea
    result <- string "-"

    getBody window #+ [column [ row [string "Transition matrix: ", element matrixInput]
                              , element result #. "result"
                              ] #. "sample-table"]

    matrixIn <- stepper "-" $ UI.valueChange matrixInput
    let readMatrix f s p = maybe "-" p . fmap s . join . fmap f . readMaybe . ("["++) . (++"]")
        readTransMatrix = readMatrix (\m -> if checkLen (length m) then Just m else Nothing)
                                     solveKolmogorovEq
                                     (concat . intersperse ", " . map (printf "%.2f"))
                          <$> matrixIn
        checkLen len = len `elem` [l^2 | l <- [1..10]]

    element result # sink text readTransMatrix

solveKolmogorovEq :: [Double] -> [Double]
solveKolmogorovEq m = concat . toLists $ solution
  where
    l = truncate . sqrt . fromIntegral . length $ m
    m' = ((l+1)><l) (replicate l 1 ++ m) -- - (diag $ fromList $ replicate l 1)
    zeros = ((l+1)><1) $ 1 : replicate l 0
    solution = linearSolveSVD m' zeros
