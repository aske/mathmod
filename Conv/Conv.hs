{-# LANGUAGE BangPatterns #-}
import           Control.Applicative
import           Control.Monad
import           Text.Read                   (readMaybe)

import           Data.List                   (intersperse)
import           Data.List
import           Debug.Trace
import qualified Graphics.UI.Threepenny      as UI
import           Graphics.UI.Threepenny.Core
import           Text.Printf

main :: IO ()
main = startGUI defaultConfig { tpPort = 10000, tpStatic = Just "." } setup

setup :: Window -> IO ()
setup window = void $ do
    return window # set title "Kolmogorov equations"
    UI.addStyleSheet window "statkolm.css"

    itemsInput <- UI.textarea
    critsInput <- UI.textarea
    resultEl <- string "-"
    criterionComparison <- string "-"
    addItemButton <- UI.button #. "button" #+ [string "click to add"]
    a <- UI.option # set text "Asus" # set value "1, 2,\n3, 4"
    b <- UI.option # set text "Samsung" # set value "0.5, 4,\n7, 4"
    list <- UI.select #+ [element  a, element b]

    getBody window #+ [column [ row [string "Item comp: ", element itemsInput
                                    , string "Criterions comp: ", element critsInput]
                              , element resultEl #. "itemComparison"
                              , element list
                              , element addItemButton
                              ] #. "sample-table"]

    itemsIn <- stepper "-" $ UI.valueChange itemsInput
    critsIn <- stepper "-" $ UI.valueChange critsInput


    on UI.click addItemButton $ const $ do
       prev <- (itemsInput # get value)
       sel <- list # get value
       if length prev < 1
       then element itemsInput # set value sel
       else element itemsInput # set value (prev ++ "," ++ "\n" ++ sel)

    let readComp :: ([Double] -> Maybe [Double]) -> String -> [Double]
        readComp f inp = maybe [] id (join . fmap f . readMaybe . ("["++) . (++"]") $ inp)
        readItems = readComp (\m -> if True then Just m else Nothing) <$> itemsIn
        checkLen len = len `elem` [l^2 | l <- [1..10]]
        readCrits = readComp (\m -> if checkLen (length m) then Just m else Nothing) <$> critsIn
        result = fmap show $ solve <$> readItems <*> readCrits

    element resultEl # sink text result

prepareComparison :: [Double] -> [[Double]]
prepareComparison cs = splitList cs (truncate . sqrt . genericLength $ cs)

splitList :: [Double] -> Int -> [[Double]]
splitList l n = let go :: [Double] -> [[Double]] -> [[Double]]
                    go list acc = if null list then reverse acc
                                     else go (drop n list) ((take n list) : acc)
                in  go l []
weights :: [[Double]] -> [Double]
weights comparison = let sums = map sum comparison
                       in  map (/ sum sums) sums

solve :: [Double] -> [Double] -> [Double]
solve _ [] = []
solve [] _ = []
solve items crits | (length items) `mod` (truncate . sqrt . genericLength $ crits) /= 0 = []
                  | otherwise = wc
  where
    ilen = truncate . sqrt . fromIntegral $ itemCompLen
    itemCompLen :: Int
    itemCompLen = truncate $ (fromIntegral $ length items) / fromIntegral clen
    clen = (slen crits)
    slen = truncate . sqrt . genericLength
    !a = trace (show wc) 0
    wc = weightedCriterions ilen clen (map (\l -> splitList l ilen) $ splitList items itemCompLen) (splitList crits clen)


weightedCriterions :: Int -> Int -> [[[Double]]] -> [[Double]] -> [Double]
weightedCriterions n m itemComparisons criterionComparison = map sum $ map (\i -> map (!! i) c) [0..n-1]
  where
    items = map (weights) itemComparisons
    ws = weights criterionComparison
    c = zipWith (\a b -> map (*b) a) items ws
