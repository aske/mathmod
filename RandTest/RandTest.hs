import           Control.Applicative
import           Control.Monad
import           Data.Maybe
import           Text.Printf
import           Text.Read                   (readMaybe)

import           Data.List
import           Data.Number.Erf
import           Graphics.Gnuplot.Simple
import qualified Graphics.UI.Threepenny      as UI
import           Graphics.UI.Threepenny.Core

main :: IO ()
main = startGUI defaultConfig { tpPort = 10000, tpStatic = Just "." } setup

files = ["true-sample", "table-sample", "gen-sample"]

mkSampleSelect :: FilePath -> IO ([Double], Element)
mkSampleSelect file = do
  sample <- (read :: String -> [Double]) <$> readFile file
  elem <- UI.select #+ (UI.option # set text "Click to show" : map (\x -> UI.option # set text (show x) # set (attr "disabled") "") sample)
  return (sample, elem)

mkSampleColumn :: String -> Element -> Element -> String -> IO Element
mkSampleColumn inInfo inEl resEl commonInfo =
  column [ grid [ [string inInfo, element inEl], [string "P-value: ", element resEl]]
         , string commonInfo
         ] #. "sample-table"

mkSampleColumnImage (inInfo, inEl, resEl, commonInfo, image, link) =
  column [ row [
         grid [ [string inInfo, element inEl]
              , [string "P-value: ", element resEl ]
              ], UI.a # set UI.href link #+ [element image] ]
         , string commonInfo
         ] #. "sample-table"

mkSampleImage = UI.image #. "sample-image"
               # set UI.height 160 # set UI.width 240

setup :: Window -> IO ()
setup window = void $ do
    return window # set title "Randomness Tester"
    UI.addStyleSheet window "randtest.css"

    randoms <- UI.input
    result <- UI.string "-"

    (sampleData, sampleSelects) <- unzip <$> mapM mkSampleSelect files
    results <- replicateM 3 (UI.string "-")

    let hist sampleData = map (\x -> (head x, fromIntegral $ length x)) (group . sort $ sampleData)
        imgFiles = ["true-rand.png", "table-rand.png", "gen-rand.png"]

    zipWithM_ (\f sampleData -> plotPathStyle [PNG f, YRange (0, 5)] (defaultStyle {plotType = Impulses}) (hist sampleData)) imgFiles sampleData
    imgUrls <- mapM (loadFile window "image/png") imgFiles
    images <- replicateM 3 mkSampleImage

    let columns = zip6 ["Random.org randoms: ", "Table randoms: ", "Generator randoms: "]
                       sampleSelects results
                       (zipWith (++) ["\"True\" random numbers, file: ", "Table random numbers, file: ", "Generated random numbers, file: "] files)
                       images imgUrls

    getBody window #+ (
      mkSampleColumn "Randoms: " randoms result "Results update while typing. \"-\" indicates malformed input or small/degenerate sample. \nP-value > 0.05 indicates that sequence is random."
      :
      map mkSampleColumnImage columns)

    randomsIn <- stepper "-" $ UI.valueChange randoms
    let withString f = maybe "-" (printf "%.2f") . join . fmap f . readMaybe
        readList f = withString f . ("["++) . (++"]")
        resultOut = readList (waldWolfowitzTest 2.0) <$> randomsIn
        getResult elem seq = element elem # set text (maybe "-" (printf "%.2f") (waldWolfowitzTest 2.0 seq))

    element result # sink text resultOut

    zipWithM_ getResult results sampleData
    zipWithM_ (\img url -> element img # set UI.src url) images imgUrls

normalCDF :: Double -> Double -> Double -> Double
normalCDF mean sd x = 0.5 * (1 + erf ((x - mean) / (sd * sqrt 2)))

waldWolfowitzTest :: Double -> [Double] -> Maybe Double
waldWolfowitzTest _ seq | null seq = Nothing
waldWolfowitzTest conf seq = if m > 2.0 then Just pValue else Nothing
  where
  n = length seq
  mean = sum seq / fromIntegral n
  seqFilt =  filter (/= mean) seq
  (lM, lP) = partition (< mean) seqFilt
  (nM, nP) = (length lM, length lP)
  runs = fromIntegral . length . group . map (compare mean) $ seqFilt
  m = fromIntegral (2 * nM * nP) / fromIntegral n + 1.0
  sigma = sqrt $ (m - 1) * (m - 2) / fromIntegral (n - 1)
  zValue = (runs - m) / sigma
  pValue = 2 * normalCDF 0 1 (- (abs zValue))
